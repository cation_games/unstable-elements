﻿using UnityEngine;

public class WaterBlendColor : MonoBehaviour {

    private SpriteRenderer spriteRenderer;

    // total droplet count so far (by collision)
    private int _dropletNum;
    // total droplets, used to ease
    private const int _totalDroplets = 7500;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // check if droplet
        if (collision.collider.tag == BasicParticleSpawn.DropletTag) {
            _dropletNum++;

            SpriteRenderer targRenderer = collision.gameObject.GetComponent<SpriteRenderer>();
            float lerp = EasingMath.Sinerp(_dropletNum, _totalDroplets);
            spriteRenderer.color = Color.Lerp(spriteRenderer.color, targRenderer.color, lerp);
        }
    }
}
