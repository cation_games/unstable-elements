﻿using System;
using UnityEngine;

/// <summary>
/// Controller that moves the beaker right/left to catch droplets.
/// </summary>
public class BeakerCatchController : MonoBehaviour {

    public BasicParticleSpawn ParticleSpawn;
    public Color particle1, particle2, particle3;
    public float Range = 4.5f;

    public EasingMath.EaseType Level1Ease = EasingMath.EaseType.SmootherStep;
    public EasingMath.EaseType Level2Ease = EasingMath.EaseType.Coserp;
    public EasingMath.EaseType Level3Ease = EasingMath.EaseType.Sinerp;
    public EasingMath.EaseType CurrentEase;

    private bool _isMoving = true;
    private bool _movRight = true;
    public int CurrentLevel = 0;

    const float COUNTDOWN = 2.5f;
    private float _movementCountdown = COUNTDOWN;

    public float LerpTime = 0.0f;
    public const float MaxTime = 4.0f;

    private void Start() {
        CurrentEase = Level1Ease;
        transform.position = new Vector2(-Range, transform.position.y);

        // don't ask
        ParticleSpawn.OnEmit = (GameObject obj) =>
        {
            SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();

            switch(CurrentLevel)
            {
                case 1:
                    renderer.color = particle1;
                    break;

                case 2:
                    renderer.color = particle2;
                    break;

                case 3:
                    renderer.color = particle3;
                    break;
            }
        };     
    }

    private void Update() {
        // if we're moving..
        if (_isMoving) {
            Move();
        }
        else {
            // wait for the particle emitter to finish
            if (ParticleSpawn.Finished()) {
                // count down, at the end - move again
                _movementCountdown -= Time.deltaTime;
                if (_movementCountdown <= 0.0f) {
                    // if the current level is 3..stop
                    if (CurrentLevel == 3)
                        enabled = false;
                    else
                        _isMoving = true;
                }
            }
        }
        
        // when the user taps the screen
        // we want to stop the beaker
        if(Input.GetButtonDown("Fire1") && _isMoving) {
            // stop moving
            _isMoving = false;
            // increase the level, spawn the correct particle..
            CurrentLevel += 1;
            // reset countdown
            _movementCountdown = COUNTDOWN;
            // begin spawning particles
            ParticleSpawn.Emit(100, 0.5f);
        }
    }

    private EasingMath.EaseType GetEaseType()
    {
        switch(CurrentLevel)
        {
            default:
            case 0:
                return Level1Ease;

            case 1:
                return Level2Ease;

            case 2:
                return Level3Ease;
        }
    }

    private void Move()
    {
        Vector2 lerpLeft = new Vector2(-Range, transform.position.y);
        Vector2 lerpRight = new Vector2(Range, transform.position.y);

        LerpTime += Time.deltaTime;

        if(LerpTime >= MaxTime) {
            // switch direction
            _movRight = !_movRight;
            // reset lerp time to 0
            LerpTime = 0.0f;
            // see if we need to change our current ease
            CurrentEase = GetEaseType();
        }

        // use level indicator to change the easing formula
        float lerp = EasingMath.EaseFromType(CurrentEase, LerpTime, MaxTime);

        // apply the lerp
        Vector2 d = (_movRight) ? lerpRight : lerpLeft;
        Vector2 s = (_movRight) ? lerpLeft : lerpRight;
        Vector2 jump = Vector2.Lerp(s, d, lerp);
        transform.position = jump;
    }
}
