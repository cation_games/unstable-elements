﻿using Assets.Scripts.Utils;
using UnityEngine;

public class BeakerCatchGoal : MonoBehaviour {

    public IncreaseWater Water;
    public BeakerCatchController BeakerController;

    static int BeakerCutoff = 40;
    public int level1Count, level2Count, level3Count;

    private void Update() {
        // calculate amounts 
        int l1_amount = level1Count;
        int l2_amount = level2Count - level1Count;
        int l3_amount = level3Count - level2Count;

        print(BeakerController.CurrentLevel);

        // track them!
        switch (BeakerController.CurrentLevel)
        {
            case 1:
                level1Count = Water.DropletsNum;
                break;

            case 2:
                level2Count = Water.DropletsNum;
                break;

            case 3:
                level3Count = Water.DropletsNum;
                break;
        }

        // calculate if we're in range of the end goal
        if(l1_amount >= BeakerCutoff && l2_amount >= BeakerCutoff && l3_amount >= BeakerCutoff) {
            WinLossUtility.Win();
            enabled = false;
        }
    }
}
