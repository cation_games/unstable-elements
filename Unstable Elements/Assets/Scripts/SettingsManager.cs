﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{


    [SerializeField] private Text _musicPercentage;
    [SerializeField] private Text _audioPercentage;

    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _audioSlider;

    private GameManager _gameManager;
    // Use this for initialization
    void Start ()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _musicSlider.value = _gameManager.AudioManager.MusicSource.volume;
        _audioSlider.value = _gameManager.AudioManager.AudioSource.volume;
        _audioPercentage.text = (int)(_audioSlider.value * 100) + "%";
        _musicPercentage.text = (int)(_musicSlider.value * 100) + "%";
    }
	
	// Update is called once per frame
	void Update () {
	   
	    
    }

    public void ChangeAudioVolume()
    {
        _gameManager.AudioManager.AudioSource.volume = _audioSlider.value;
        _audioPercentage.text = (int)(_audioSlider.value * 100) + "%";

    }

    public void ChangeMusicVolume()
    {
        _gameManager.AudioManager.MusicSource.volume = _musicSlider.value;
        _musicPercentage.text = (int)(_musicSlider.value * 100) + "%";
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GetComponent<Animator>().SetTrigger("Close");
    }
}
