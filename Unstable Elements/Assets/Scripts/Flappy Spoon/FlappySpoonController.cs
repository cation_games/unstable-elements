﻿using Assets.Scripts.Utils;
using UnityEngine;

public class FlappySpoonController : MonoBehaviour
{
    private bool isDead = false;            //Has the player collided with a wall?

    public AudioClip Float, Hit;
    private AudioSource _player;

    public Vector2 force;
    private Rigidbody2D rb2d;               //Holds a reference to the Rigidbody2D component of the bird.

    void Start()
    {
        //Get and store a reference to the Rigidbody2D attached to this GameObject.
        rb2d = GetComponent<Rigidbody2D>();
        _player = GetComponent<AudioSource>();
    }

    void Update()
    {
        //Don't allow control if the bird has died.
        if (!isDead) {
            //Look for input to trigger a "flap".
            if (Input.GetMouseButtonDown(0)) {
                //...zero out the birds current y velocity before...
                rb2d.velocity = Vector2.zero;
                rb2d.velocity = force;

                _player.clip = Float;
                _player.Play();
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "KillCollider") {
            if (!isDead) {
                _player.clip = Hit;
                _player.Play();
            }

                // Zero out the bird's velocity
                rb2d.velocity = Vector2.zero;
            // If the bird collides with something set it to dead...
            isDead = true;

            WinLossUtility.Lose();
        }
        else if (other.gameObject.tag == "Goal") {
            if (isDead) return;

            // we won!
            WinLossUtility.Win();
            isDead = true;

            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }
}
