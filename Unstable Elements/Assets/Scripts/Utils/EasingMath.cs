﻿using System;

/// <summary>
/// Utilities class for better lerp functions and other math based stuff.
/// </summary>
public static class EasingMath
{
    /// <summary>
    /// Defines the type of easing.
    /// </summary>
    public enum EaseType
    {
        Sinerp,
        Coserp,
        Quadtratic,
        SmoothStep,
        SmootherStep
    }

    /// <summary>
    /// Eases using the default curr/lerp time values, using the function
    /// represented by the EaseType enum.
    /// </summary>
    public static float EaseFromType(EaseType type, float currTime, float lerpTime)
    {
        switch (type)
        {
            case EaseType.Sinerp:
                return Sinerp(currTime, lerpTime);
            case EaseType.Coserp:
                return Coserp(currTime, lerpTime);
            case EaseType.Quadtratic:
                return Quadtratic(currTime, lerpTime);
            case EaseType.SmoothStep:
                return SmoothStep(currTime, lerpTime);
            case EaseType.SmootherStep:
                return SmootherStep(currTime, lerpTime);
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }
    }

    public static float Sinerp(float currTime, float lerpTime)
    {
        var t = currTime / lerpTime;
        return (float)Math.Sin(t * Math.PI * 0.5f);
    }

    public static float Coserp(float currTime, float lerpTime)
    {
        var t = currTime / lerpTime;
        return (float)(1f - Math.Cos(t * Math.PI * 0.5f));
    }

    public static float Quadtratic(float currTime, float lerpTime)
    {
        float t = currTime / lerpTime;
        return t * t;
    }

    public static float SmoothStep(float currTime, float lerpTime)
    {
        float t = currTime / lerpTime;
        return t * t * (3f - 2f * t);
    }

    public static float SmootherStep(float currTime, float lerpTime)
    {
        float t = currTime / lerpTime;
        return t * t * t * (t * (6f * t - 15f) + 10f);
    }
}
