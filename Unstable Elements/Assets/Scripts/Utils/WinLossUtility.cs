﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// BABIS! 
    /// Put your code here!
    /// </summary>
    public class WinLossUtility
    {
        public static void Win()
        {
            GameManager manager = Object.FindObjectOfType<GameManager>();
            if (manager != null)
                manager.WinGame();
            else
                Debug.Log("Tried to win, but game manager did not exist");
        }

        public static void Lose()
        {
            GameManager manager = Object.FindObjectOfType<GameManager>();
            if (manager != null)
                manager.LoseGame();
            else
                Debug.Log("Tried to lose, but game manager did not exist");
        }
    }
}
