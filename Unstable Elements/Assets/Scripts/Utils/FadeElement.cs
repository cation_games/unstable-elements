﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Attach this to an image in order to fade it out in the given amount of time.
/// </summary>
public class FadeElement : MonoBehaviour {

    // Result of the fading ending
    [System.Serializable]
    public enum EndResult : byte {
        NONE = 1,
        DESTROY_COMPONENT = 2,
        DESTROY_OBJECT = 4,
        DISABLE = 8
    }

    // Inspector settings
    public float FadeTime = 1.0f;
    public EndResult TargetResult = EndResult.DISABLE;
    public EndResult SelfResult = EndResult.DISABLE;
    public EasingMath.EaseType EaseFormula = EasingMath.EaseType.SmootherStep;

    // Target settings
    private float _time = 0.0f;
    private Image _img;

    private void Start()
    {
        _img = GetComponent<Image>();
        Debug.Assert(_img != null, "FadeElement component must be attached to an image.");
        Debug.Assert(FadeTime <= 0.0f, "FadeTime cannot be 0 or less.");

        // disable self, not destroy for debug purposes
        if (_img == null)
            enabled = false;

        _time = FadeTime;
    }

    private void Update()
    {
        // Count down time
        _time -= Time.deltaTime;

        // if we're at the end, stop
        if(_time <= 0.0f) {
            Complete();
            // reset for lerp
            _time = 0.0f;
        }

        // lerp between current & max
        float lerp = EasingMath.EaseFromType(EaseFormula, _time, FadeTime);

        // apply to color
        Color imgColor = _img.color;
        imgColor.a = lerp;
        _img.color = imgColor;
    }

    private void Complete()
    {
        // Apply result to target
        switch (TargetResult) {
            case EndResult.DESTROY_COMPONENT:
                Destroy(_img);
                break;

            case EndResult.DESTROY_OBJECT:
                Destroy(_img.gameObject);
                return; //return here, as this destroys the fade component also

            case EndResult.DISABLE:
                _img.enabled = false;
                break;
        }

        // Apply result to self
        switch (SelfResult)
        {
            case EndResult.DESTROY_COMPONENT:
                Destroy(this);
                break;

            case EndResult.DESTROY_OBJECT:
                Destroy(gameObject);
                return; //return here, as this destroys the fade component also

            // Same for none & disable, stop the counter
            case EndResult.NONE:
            case EndResult.DISABLE:
                enabled = false;
                break;
        }
    }
}
