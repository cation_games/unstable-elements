﻿using System;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class OffsetObject : MonoBehaviour {

    public float MovementTime = 1.0f;
    public float MaxSpeed = 4.0f;

    public float _time;
    private float _speed;

    public bool moveLeft;
    public Vector2 RangeBounds = new Vector2(1, 0);

    private Vector3 _leftRange, _rightRange;

    private void Start() {
        _speed = UnityEngine.Random.Range(2.0f, MaxSpeed);
        _time = MovementTime;
        _leftRange = transform.position - new Vector3(RangeBounds.x, RangeBounds.y, 0);
        _rightRange = transform.position + new Vector3(RangeBounds.x, RangeBounds.y, 0);
    }

    private void Update()
    {
        MovementTime -= Time.deltaTime;

        if (MovementTime > 0.0f) {
            Move();
        }
        else
            enabled = false;
    }

    private void Move()
    {
        // next jump speed
        float speed = _speed * Time.deltaTime;
        // distance to target
        float targdst = Vector2.Distance(transform.position, (moveLeft) ? _leftRange : _rightRange);

        // if under next dst, switch direction
        if(targdst < speed) {
            moveLeft = !moveLeft;
        }

        // calcs velocity, applies our speed & dt scalar
        Vector2 velocity = (moveLeft) ? new Vector2(-1, 0) : new Vector2(1, 0);
        velocity *= speed;

        transform.position += new Vector3(velocity.x, velocity.y, 0);
    }
}
