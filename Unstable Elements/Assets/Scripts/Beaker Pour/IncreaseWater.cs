﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public class IncreaseWater : MonoBehaviour {

    private BoxCollider2D boxCollider;
    private SpriteRenderer sprRenderer;

    public BasicParticleSpawn ParticleParent;
    public float YIncrement = 0.025f;
    public int DropletsNum = 0;

    private Vector3 _targetScale;

    private void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        sprRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // check it's a droplet
        if(collision.collider.tag == BasicParticleSpawn.DropletTag) {
            // recycle droplet
            ParticleParent.Recycle(collision.gameObject);

            // increase num of droplets
            DropletsNum++;

            // increase water, move to compensate
            transform.localScale += new Vector3(0, YIncrement, 0);
            
            // and scale the collider to match
            Vector3 sprBounds = sprRenderer.sprite.bounds.size;
            boxCollider.size = sprBounds;
        }
    }

}
