﻿using System;
using UnityEngine;

public class LineColliderController : MonoBehaviour {

    private bool pressed = false;
    private Vector3 _lineStart;
    private LineRenderer _line;

    public Material LineMaterial;
    public float LineThickness = 0.25f;
    public Color LineColor = new Color(1, 1, 1, 1);

    private AudioSource source;
    public AudioClip Clip;

    private void Start()
    {
        _line = gameObject.AddComponent<LineRenderer>();

        _line.startWidth = LineThickness;
        _line.endWidth = LineThickness;

        _line.startColor = LineColor;
        _line.endColor = LineColor;

        _line.material = LineMaterial;

        source = gameObject.AddComponent<AudioSource>();
        source.clip = Clip;
    }

    private void Update()
    {
        if(!pressed && Input.GetButton("Fire1"))
        {
            print("Beginning line drawing");

            // not touch friendly, rn
            _lineStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            source.Play();

            pressed = true;
        }
        else if(pressed)
        {
            // has been pressed
            Vector3 lineEnd = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            _lineStart.z = transform.position.z;
            lineEnd.z = _lineStart.z;

            _line.SetPositions( new Vector3[] { _lineStart, lineEnd } );

            if(!Input.GetButton("Fire1")) {
                enabled = false;
                print("lines complete");
                MakeCollider(_lineStart, lineEnd);
            }
        }
    }

    private void MakeCollider(Vector3 start, Vector3 end)
    {
        GameObject childCollider = new GameObject();
        childCollider.transform.parent = transform;

        var collider = childCollider.AddComponent<BoxCollider2D>();

        float length = Vector2.Distance(start, end);
        collider.size = new Vector2(length, LineThickness);

        Vector3 mid = (start + end) / 2;
        collider.transform.position = mid;

        float angle = (Mathf.Abs(start.y - end.y)) / (Mathf.Abs(start.x - end.x));

        if ((start.y < end.y && start.x > end.x) || (end.y < start.y && end.x > start.x))
        {
            angle *= -1;
        }

        angle = Mathf.Rad2Deg * Mathf.Atan(angle);
        collider.transform.Rotate(0, 0, angle);
    }
}
