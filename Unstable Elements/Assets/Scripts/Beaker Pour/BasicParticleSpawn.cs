﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BasicParticleSpawn : MonoBehaviour {

    public static string DropletTag = "Droplet";

    // Settings..
    public GameObject ParticlePrefab;
    public int NumParticles = 1000;
    public float Delay = 1.0f;
    public int NumSpawned = 0;

    public float RandomRangeModifier = 0.5f;

    // Pooling..
    private Queue<GameObject> _poolQueue;
    private List<GameObject> _active;

    /// <summary>
    /// Called when a gameObject is emitted.
    /// </summary>
    public Action<GameObject> OnEmit;

    private void Awake()
    {
        _poolQueue = new Queue<GameObject>();
        _active = new List<GameObject>();

        // spawn some items and put them into the pool
        for(int i = 0; i < NumParticles/4; i++) {
            GameObject go = Instantiate(ParticlePrefab);
            go.SetActive(false);
            _poolQueue.Enqueue(go);
        }
    }

    private void Update()
    {
        Delay -= Time.deltaTime;

        if(Delay <= 0 && NumSpawned < NumParticles) {
            SpawnParticles();
        }
    }

    private void SpawnParticles() {
        int rndgen = UnityEngine.Random.Range(1, 3);

        if (NumSpawned + rndgen > NumParticles)
            rndgen = NumParticles - NumSpawned;

        NumSpawned += rndgen;

        for(int i = 0; i < rndgen; i++) {
            GameObject obj =  (_poolQueue.Count != 0) ? _poolQueue.Dequeue() : Instantiate(ParticlePrefab);

            obj.SetActive(true);
            obj.tag = DropletTag;

            // offset position randomly, around a half unit
            obj.transform.position = transform.position + 
                new Vector3(UnityEngine.Random.Range(-RandomRangeModifier, RandomRangeModifier), 
                UnityEngine.Random.Range(-RandomRangeModifier, RandomRangeModifier), 0);

            if (OnEmit != null)
                OnEmit(obj);
        }
    }

    public void Recycle(GameObject gameObject)
    {
        // disable the object
        gameObject.SetActive(false);
        // queue the gameobject for reuse
        _poolQueue.Enqueue(gameObject);
    }

    public void Emit(int amount, float delay, bool reset = true) {
        Delay           = reset ? delay  : Delay + delay;
        NumParticles    = reset ? amount : NumParticles + amount;
        NumSpawned      = reset ? 0      : NumSpawned;

        // enable the emitter
        enabled = true;
    }

    public bool Finished()
    {
        return NumSpawned >= NumParticles;
    }
}
