﻿using Assets.Scripts.Utils;
using UnityEngine;

public class BeakerPourWinCondition : MonoBehaviour {

    public IncreaseWater Water;
    public BasicParticleSpawn ParticleSpawn;

    public float Countdown = 20.0f;

    private void Update()
    {
        Countdown -= Time.deltaTime;

        if(Countdown <= 0.0f) {
            // OK! Calculate what we got.
            float perc = ((float)Water.DropletsNum / (float)ParticleSpawn.NumParticles) * 100.0f;
            // Check what we have
            
            if(perc > 60.0f) {
                WinLossUtility.Win();
            }
            else {
                WinLossUtility.Lose();
            }

            //don't run again
            enabled = false;
        }

    }

}
