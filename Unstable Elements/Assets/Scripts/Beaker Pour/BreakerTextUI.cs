﻿using UnityEngine;
using UnityEngine.UI;

public class BreakerTextUI : MonoBehaviour {

    public BasicParticleSpawn Emitter;
    public IncreaseWater Water;

    private Text _text;

    private void Start()
    {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update () {
        // avoid div by 0 error
        if (Water.DropletsNum == 0 || Emitter.NumParticles == 0) return;

        // calc perc, display as score
        float perc = ((float)Water.DropletsNum / (float)Emitter.NumParticles) * 100.0f;
        _text.text = string.Format("SCORE: {0}/100", Mathf.Round(perc));
	}
}
