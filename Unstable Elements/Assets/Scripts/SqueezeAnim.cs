﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SqueezeAnim : MonoBehaviour {

    private const float lerpTime = 0.2f;
    private float _time;
    public bool pressed = false;
    public bool animBack = false;

    private Vector3 startScale;
    private Vector3 startRotation;

    public Vector3 LerpRotate = new Vector3(0, 0, 0);
    public Vector3 LerpScale = new Vector3(0.0f, 0.0f, 0.0f);

    private void Awake()
    {
        startScale = transform.localScale;
        startRotation = transform.rotation.eulerAngles;
    }

    private void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(() => {
            pressed = true;
            print("DANCE!!");
        });

        print("Woke up");
    }

    private void Update()
    {
        if (pressed)
        {
            _time += Time.deltaTime;

            if (_time >= lerpTime)
            {
                _time = 0.0f;
                pressed = false;
                animBack = true;
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(LerpRotate), _time);
            transform.localScale = Vector3.Lerp(transform.localScale, LerpScale, _time);

            print("PRESSED?");
        }

        if (animBack)
        {
            _time += Time.deltaTime;

            if (_time >= lerpTime)
            {
                _time = 0.0f;
                animBack = false;
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(startRotation), _time);
            transform.localScale = Vector3.Lerp(transform.localScale, startScale, _time);
        }
    }

}
