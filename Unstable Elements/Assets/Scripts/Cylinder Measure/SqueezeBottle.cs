﻿using UnityEngine;

public class SqueezeBottle : MonoBehaviour {
    public BasicParticleSpawn Particles;
    private bool inBounds = false;

    public Vector3 LerpRotate = new Vector3(0, 0, 50);
    public Vector3 LerpScale = new Vector3(0.20f, 0.20f, 0.20f);

    private const float lerpTime = 0.2f;
    private float _time;
    public bool pressed = false;
    public bool animBack = false;

    public AudioClip Clip;
    private AudioSource audioSource;

    private Vector3 startScale;
    private Vector3 startRotation;

    private void Awake()
    {
        startScale = transform.localScale;
        startRotation = transform.rotation.eulerAngles;

        audioSource = gameObject.AddComponent<AudioSource>();
    }

    private void Emit() {
        Particles.RandomRangeModifier = 0.1f;
        Particles.Emit(1, 0.0f, false);

        if(!pressed && !animBack) {
            _time = 0.0f;
        }

        if(!audioSource.isPlaying)
        {
            audioSource.clip = Clip;
            audioSource.Play();
        }

        pressed = true;
    }

    private void Update() {
        if (inBounds && Input.GetMouseButton(0))
            Emit();

        if(pressed) {
            _time += Time.deltaTime;

            if(_time >= lerpTime) {
                _time = 0.0f;
                pressed = false;
                animBack = true;
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(LerpRotate), _time);
            transform.localScale = Vector3.Lerp(transform.localScale, LerpScale, _time);
        }

        if(animBack) {
            _time += Time.deltaTime;

            if (_time >= lerpTime) {
                _time = 0.0f;
                animBack = false;
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(startRotation), _time);
            transform.localScale = Vector3.Lerp(transform.localScale, startScale, _time);
        }
    }

    private void OnMouseEnter() {
        inBounds = true;
    }

    private void OnMouseExit() {
        inBounds = false;
    }
}
