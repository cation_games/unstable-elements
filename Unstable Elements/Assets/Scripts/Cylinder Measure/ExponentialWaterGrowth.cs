﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Utils;
using UnityEngine;

public class ExponentialWaterGrowth : MonoBehaviour {

    private BoxCollider2D boxCollider;
    private SpriteRenderer sprRenderer;

    public BasicParticleSpawn ParticleParent;
    private Vector3 _targetScale;

    private float YGrowth = 0.25f / 10;
    private float YGrowthModifier = 0.0125f / 10;

    private Vector2 targetScale;

    private float winTimer = 0.5f;

    public AudioClip Clip;
    private AudioSource source;

    private void Awake()
    {
        source = gameObject.AddComponent<AudioSource>();
    }

    private void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        sprRenderer = GetComponent<SpriteRenderer>();

        targetScale = transform.localScale;
    }

    private void Update()
    {
        // if we need to scale up
        if(targetScale.y > transform.localScale.y) {
            Vector2 scale = transform.localScale;
            scale.y += (12 * Time.deltaTime);
            scale.y = Mathf.Clamp(scale.y, transform.localScale.y, targetScale.y);
            transform.localScale = scale;

        }

        // scale 12 is the bottom of the green line
        if (transform.localScale.y > 12)
        {
            if(transform.localScale.y > 12.9) {
                // you lose.
                WinLossUtility.Lose();
                enabled = false;
            }
            else {
                // we're in the win condition, so count time..
                winTimer -= Time.deltaTime;
                if(winTimer <= 0) {
                    WinLossUtility.Win();
                    enabled = false;
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // check it's a droplet
        if (collision.collider.tag == BasicParticleSpawn.DropletTag)
        {
            // recycle droplet
            ParticleParent.Recycle(collision.gameObject);

            // increase water, move to compensate
            targetScale += new Vector2(0, YGrowth);
            YGrowth += YGrowthModifier;

            // and scale the collider to match
            Vector3 sprBounds = sprRenderer.sprite.bounds.size;
            boxCollider.size = sprBounds;

            if(!source.isPlaying) {
                source.clip = Clip;
                source.Play();
            }
        }
    }

}
