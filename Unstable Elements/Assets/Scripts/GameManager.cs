﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets
{
    public class GameManager : MonoBehaviour {

        public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
        private int _livesLeft = 5;
        public AudioManager AudioManager;
        public UIManager UiManager;
        private bool begin;

        //Awake is always called before any Start functions
        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)
            {
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);
                return;
            }

            begin = true;

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

            UiManager = GetComponentInChildren<UIManager>();
            AudioManager = GetComponentInChildren<AudioManager>();

            //Call the InitGame function to initialize the first level 
            InitGame();
        }

        //Initializes the game for each level.
        void InitGame()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            int index = SceneManager.GetActiveScene().buildIndex;
            AudioManager.OnSceneLoaded(index);
           
            switch (index)
            {
                case 0:
                    UiManager.LivesRepository.SetActive(false);
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    StartCoroutine(Tutorial(index));
                    UiManager.LivesRepository.SetActive(true);
                    break;

            }


        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if(!begin) return;
            AudioManager.OnSceneLoaded(scene.buildIndex);
            switch (scene.buildIndex)
            {
                case 9:
                case 0:
                    UiManager.LivesRepository.SetActive(false);
                    UiManager.PauseButton.gameObject.SetActive(false); 
                    UiManager.ResetTutorials();
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    UiManager.ResetTutorials();
                    StartCoroutine(Tutorial(scene.buildIndex));
                
                    UiManager.LivesRepository.SetActive(true);
                    UiManager.PauseButton.gameObject.SetActive(true);
                    break;
            }

        }


        public void WinGame()
        {
            StartCoroutine(WinGameCoroutine());
        }

        private IEnumerator Tutorial(int index)
        {
            UiManager.OpenTutorial(index - 1);
            Time.timeScale = 0;
            yield return new WaitForSecondsRealtime(4);
            if(index != 8)
            Time.timeScale = 1;
        }

        IEnumerator WinGameCoroutine()
        {
            UiManager.OpenUI(0);
            Time.timeScale = 0;
            AudioManager.MusicSource.Stop();
            AudioManager.Victory();
            yield return new WaitForSecondsRealtime(3);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
            UiManager.ResetUI();
            Time.timeScale = 1;
        }

        public void LoseGame()
        {
            StartCoroutine(LoseGameCoroutine());
        }

        IEnumerator LoseGameCoroutine()
        {
            UiManager.OpenUI(1);
            Time.timeScale = 0;
            UiManager.DecrementLife(_livesLeft);
            _livesLeft--;
            AudioManager.MusicSource.Stop();
            AudioManager.LostLife();
            yield return new WaitForSecondsRealtime(3);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            UiManager.ResetUI();
            _livesLeft--;
            Time.timeScale = 1;
        }

        public void TimeUp()
        {
            StartCoroutine(TimeUpCoroutine());
        }

        IEnumerator TimeUpCoroutine()
        {
            UiManager.OpenUI(2);
            UiManager.DecrementLife(_livesLeft);
            _livesLeft--;
            Time.timeScale = 0;
            AudioManager.OutOfTime();
            AudioManager.MusicSource.Stop();
            yield return new WaitForSecondsRealtime(3);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            UiManager.ResetUI();
            Time.timeScale = 1;
        }
       
        // Update is called once per frame
        void Update () {
            if (Input.GetKeyDown(KeyCode.P))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }

            if (_livesLeft <= 0)
            {
                _livesLeft = 5;
            // Application.Quit();
               
            }

        }
    }
}
