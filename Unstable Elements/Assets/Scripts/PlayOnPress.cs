﻿using UnityEngine;
using UnityEngine.UI;

public class PlayOnPress : MonoBehaviour {

    private Button button;
    public AudioClip Clip;
    private AudioSource Source;

    private void Awake()
    {
        button = GetComponent<Button>();
        Source = GetComponent<AudioSource>();

        if (Source == null)
            Source = gameObject.AddComponent<AudioSource>();

        button.onClick.AddListener(() =>
           {
               Source.clip = Clip;
               Source.Play();
           }
        );
    }
}
