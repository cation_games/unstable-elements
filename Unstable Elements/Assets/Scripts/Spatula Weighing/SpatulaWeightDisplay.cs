﻿using UnityEngine;
using UnityEngine.UI;

public class SpatulaWeightDisplay : MonoBehaviour {

    private Text textBox;
    public SpatulaLogic logic;

    private void Start()
    {
        textBox = GetComponent<Text>();
    }

    private void Update()
    {
        textBox.text = "0." + logic.Weight.ToString();   
    }
}
