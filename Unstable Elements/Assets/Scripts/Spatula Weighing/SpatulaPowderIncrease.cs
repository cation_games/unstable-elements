﻿using UnityEngine;
using UnityEngine.UI;

public class SpatulaPowderIncrease : MonoBehaviour {

    private Image img;
    public SpatulaLogic logic;


    private void Awake()
    {
        img = GetComponent<Image>();
    }

    private void Update()
    {
        // scale img depending on the amount of powder, clamp to 2f
        float scale = 1.0f + ((float)logic.Weight / 100.0f);
        scale = Mathf.Clamp(scale, 1.0f, 2.0f);
        img.transform.localScale = new Vector2(scale, scale);
    }

}
