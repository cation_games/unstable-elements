﻿using UnityEngine;
using UnityEngine.UI;

public class Spatula : MonoBehaviour {
    public enum Type {
        BIG, LITTLE, MID
    }

    public Type type;
    public SpatulaLogic logic;
    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener( () =>
        {
            logic.Add(type);
        });
    }
}
