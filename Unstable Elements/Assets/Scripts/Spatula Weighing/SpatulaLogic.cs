﻿using System;
using Assets.Scripts.Utils;
using UnityEngine;

public class SpatulaLogic : MonoBehaviour {

    public int Weight = 0;
    public int WeightTarget = 75;

    internal void Add(Spatula.Type type) {
        if (!enabled) return;

        switch (type)
        {
            case Spatula.Type.BIG:
                Weight += 20;
                break;
            case Spatula.Type.LITTLE:
                Weight += 5;
                break;
            case Spatula.Type.MID:
                Weight += 10;
                break;
        }

        if (Weight == WeightTarget)
        {
            enabled = false;
            WinLossUtility.Win();
        }
        if (Weight > WeightTarget)
        {
            enabled = false;
            WinLossUtility.Lose();
        }
    }
}
