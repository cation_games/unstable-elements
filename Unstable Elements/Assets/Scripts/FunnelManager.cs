﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;

public class FunnelManager : MonoBehaviour
{
    [SerializeField] private GameObject _liquid;
    [SerializeField] private Animator _funnelTapAnimator;
    [SerializeField] private ParticleSystem _particleSystem;

    private bool _paused, _finished;
	// Use this for initialization
	void Start () {
	    Time.timeScale = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void FunnelTapButton()
    {
        if (_paused)
        {
            _paused = false;
            Time.timeScale = 0;
            _particleSystem.Stop();
            if (_liquid.transform.localPosition.y > -80)
            {
                
            }
            else if (_liquid.transform.localPosition.y > -100)
            {
                FindObjectOfType<GameManager>().WinGame();
                Debug.Log("VICTORY");
            }
            else
            {
                FindObjectOfType<GameManager>().LoseGame();
                Debug.Log("LOSS");
            }


        }
        else
        {
            _paused = true;
            Time.timeScale = 1;
            _particleSystem.Play();
        }
        _funnelTapAnimator.GetComponent<Animator>().SetBool("Open", _paused);
       
    }
}
