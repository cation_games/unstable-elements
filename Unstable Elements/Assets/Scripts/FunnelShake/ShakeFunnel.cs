﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ShakeFunnel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector2 _originalPosition = -Vector2.one; //Used to store location of screen touch origin for mobile controls.
    [SerializeField] private float KillSpeed;
    [SerializeField] private float RotationSpeed;
    [SerializeField] private float RotationKillSpeed;
    [SerializeField] private float ShakingSpeed;
    [SerializeField] private AudioClip _beakerSmash;
    [SerializeField] private AudioClip _shakeBooty;
    [SerializeField] private AudioClip _shakeSignora;
    public Animator liquidAnimator;
    private Rigidbody2D _rb2D;
    private Vector2 Velocity;
    private bool _grabbed, _onGround, _shaking, _finished;
    private AudioSource _audioSource;
    [SerializeField] private Image _liquidImage;
    [SerializeField] private float percentage = 50;
    // Use this for initialization
    void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();
        _rb2D.velocity = new Vector2(0, 0);
        _rb2D.angularVelocity = 0;
        _audioSource = GetComponent<AudioSource>();
        Time.timeScale = 0;
        int temp = Random.Range(0, 2);
        _audioSource.clip = temp == 0 ? _shakeBooty : _shakeSignora;
    }

    // Grab object
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_finished) return;
        _grabbed = true;
        _onGround = false;
    }

    // Stop existing velocity and gravity
    public void OnDrag(PointerEventData eventData)
    {
        if (_finished) return;
        _rb2D.velocity = new Vector2(0, 0);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_finished) return;
        // Reactivate gravity and stop grabbing object
        _grabbed = false;
        // Continue moving the object 
        if (_onGround)
            _rb2D.velocity = Velocity;
        else
            _rb2D.velocity = Velocity * 15;
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        if (_finished) return;
        // Ensure Object stays at mouse position
        _liquidImage.transform.localScale = new Vector3(_liquidImage.transform.localScale.x, percentage / 100, _liquidImage.transform.localScale.z); 
      
        if (_grabbed)
        {
            transform.position = Input.mousePosition;
           if (!_audioSource.isPlaying)
                _audioSource.Play();
        }
        else
        {
            if (_audioSource.isPlaying)
                _audioSource.Pause();
            if (Math.Abs(Velocity.x) > 0.5f || Math.Abs(Velocity.y) > 0.5f)
                _rb2D.AddForce(new Vector2(Velocity.x * 20, GetComponent<Rigidbody2D>().velocity.y));
        }
        Velocity = new Vector2(transform.position.x, transform.position.y) - _originalPosition;

        // Rotate the beaker by its current velocity 
        if (Math.Abs(Velocity.x) > 0.5f)
        {
            transform.Rotate(new Vector3(0, 0, -Velocity.x / RotationKillSpeed));
        }

        if (Math.Abs(transform.localEulerAngles.z) > 0.5f)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y,
                Mathf.LerpAngle(transform.localEulerAngles.z, 0, RotationSpeed * Time.deltaTime));
            _originalPosition = transform.position;
        }

        if (Math.Abs(Velocity.x) > ShakingSpeed || Math.Abs(Velocity.y) > ShakingSpeed)
        {
            _shaking = true;
            percentage++;
           
        }
        else
        {
            _shaking = false;
            percentage-=2;
        }
        if (percentage < 0)
            percentage = 0;
        if (percentage > 100)
        {
            _finished = true;
            percentage = 100;
            FindObjectOfType<GameManager>().WinGame();
        }

        // If on the ground freeze rotation to avoid the trembling problem
            _rb2D.freezeRotation = _onGround;

     
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (_finished) return;
        //If you hit the floor or the scale settle down
        if (collision.transform.name == "Desk" || collision.transform.name == "Scale")
        {
            if (Velocity.x > KillSpeed || Velocity.y > KillSpeed)
            {
                GetComponent<AudioSource>().PlayOneShot(_beakerSmash);
                FindObjectOfType<GameManager>().LoseGame();
                _finished = true;
            }
            _rb2D.velocity = Vector2.zero;
            _rb2D.angularVelocity = 0;
            _onGround = true;
        }
        // If you hit a wall you will be pushed out
        else if (collision.transform.name == "Wall")
        {
            if (Velocity.x > KillSpeed || Velocity.y > KillSpeed)
            {
                GetComponent<AudioSource>().PlayOneShot(_beakerSmash);
               FindObjectOfType<GameManager>().LoseGame();
            }
            _rb2D.AddForce(new Vector2(-Velocity.x * 2, GetComponent<Rigidbody2D>().velocity.y));
            _grabbed = false;
        }
       
    }

    // Reset scene
    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}


