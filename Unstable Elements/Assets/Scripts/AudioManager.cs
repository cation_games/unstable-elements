﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    // SFX
    [SerializeField] private AudioClip _gameFail;
    [SerializeField] private AudioClip _gameComplete;
    [SerializeField] private AudioClip _timeUp;
    [SerializeField] private AudioClip _applause;
    [SerializeField] private AudioClip _buttonClick;


    // MUSIC
    [SerializeField] private AudioClip _menuMusic;
    [SerializeField] private AudioClip _ambientMusic;
    [SerializeField] private AudioClip _funkyMusic;
    [SerializeField] private AudioClip _cheerfulMusic;

    // VOICE
    [SerializeField] public AudioClip Chemicool;
    [SerializeField] public AudioClip Explosive;
    [SerializeField] public AudioClip Great;
    [SerializeField] public AudioClip WhatAReaction;
    [SerializeField] public AudioClip Youach;

    public AudioSource MusicSource;
    public AudioSource AudioSource;

    public Button[] Buttons;

    // called second
    public void OnSceneLoaded(int buildIndex)
    {
       
        Buttons = FindObjectsOfType<Button>();
        foreach (var button in Buttons)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => AudioSource.PlayOneShot(_buttonClick));
        }

        switch (buildIndex)
        {
            case 0:
                MusicSource.clip = _menuMusic;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                MusicSource.clip = _funkyMusic;
                break;
            case 7:
                MusicSource.clip = _ambientMusic;

                break;
            case 8:
                MusicSource.clip = _funkyMusic;
                break;

        }
        MusicSource.Play();

    }

    // Use this for initialization
    void Start () {
     
        switch (SceneManager.GetActiveScene().buildIndex)
        {
       
            case 0:
                MusicSource.clip = _menuMusic;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                MusicSource.clip = _funkyMusic;
                break;
            case 7:
                MusicSource.clip = _ambientMusic;

                break;
            case 8:
                MusicSource.clip = _funkyMusic;
                break;
          
        }
        Debug.Log(SceneManager.GetActiveScene().buildIndex);
        MusicSource.Play();
        Buttons = FindObjectsOfType<Button>();
        foreach (var button in Buttons)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => AudioSource.PlayOneShot(_buttonClick));
        }
    }

    public void LostLife()
    {
        AudioSource.PlayOneShot(_gameFail);
    }

    public void OutOfTime()
    {
        AudioSource.PlayOneShot(_timeUp);
    }

    public void Victory()
    {
        AudioSource.PlayOneShot(_gameComplete);
    }
}
