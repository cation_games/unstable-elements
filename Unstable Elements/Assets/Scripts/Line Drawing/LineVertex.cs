﻿using UnityEngine;
using UnityEngine.EventSystems;

public class LineVertex : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerEnterHandler
{
    [HideInInspector]
    public DrawVerticesController Controller = null;

    // The vertex that we are connected too
    // this is used by the line renderer
    public LineVertex connected = null;

    // The target to join up the lines
    public LineVertex joinTarget = null;

    public void OnDrag(PointerEventData eventData)
    {
        Controller.OnDrag(this, eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Controller.OnEndDrag(this, eventData);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Controller.OnPointerEnter(this, eventData);
    }

    private void Start()
    {
        Debug.Assert(connected != null || joinTarget != null, 
            "Line vertex must either be connected or have a join target.");    
    }
}
