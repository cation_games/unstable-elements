﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// Controller for handling drawing from vertex to vertex.
/// Expects a set of child GameObjects with LineVertex components.
/// </summary>
public class DrawVerticesController : MonoBehaviour
{
    private List<LineVertex> _vertices;

    // Touch & movement 
    LineVertex _dragVertex;
    LineVertex _originJoinVertex;
    LineRenderer _joinLineRenderer;

    // Line settings 
    public Color LineColor;
    public Material LineMaterial;
    [Range(0.1f, 100.0f)]
    public float LineWidth;

    private void Start()
    {
        LineVertex[] children = transform.GetComponentsInChildren<LineVertex>();
        _vertices = new List<LineVertex>(children);

        Debug.Assert(children.Length > 1, 
            "DrawVerticesController expects at least two or more children with LineVertex components.");

        // create & connect our line rendering
        foreach(LineVertex vertex in _vertices) {
            // assign self
            vertex.Controller = this;

            // Create a line renderer from A to B
            var lineRenderer = vertex.gameObject.AddComponent<LineRenderer>();

            // set width
            lineRenderer.startWidth = LineWidth;
            lineRenderer.endWidth = LineWidth;

            // set color
            lineRenderer.material = LineMaterial;
            lineRenderer.startColor = LineColor;
            lineRenderer.endColor = LineColor;

            Vector3 targetPosition;

            if(vertex.connected != null) {
                targetPosition = vertex.connected.transform.position;
            }
            else {
                targetPosition = transform.position;
                _originJoinVertex = vertex;
                _joinLineRenderer = lineRenderer;
            }

            // set the positions of the line renderer
            lineRenderer.SetPositions(new Vector3[] {
                vertex.transform.position, targetPosition
            });
        }

        Debug.Assert(_originJoinVertex != null, "DrawVerticesController requires an origin join vertex.");
    }

    public void OnEndDrag(LineVertex lineVertex, PointerEventData eventData)
    {
        if (!enabled) return;

        if (lineVertex.joinTarget != null) {
            _joinLineRenderer.SetPosition(1, lineVertex.transform.position);
        }
    }

    public void OnDrag(LineVertex lineVertex, PointerEventData eventData)
    {
        if (!enabled) return;

        _dragVertex = lineVertex;

        if (lineVertex.joinTarget != null) {
            // move the line to the correct position
            var worldpoint = Camera.main.ScreenToWorldPoint(eventData.position);
            worldpoint.z = lineVertex.transform.position.z;
            _joinLineRenderer.SetPosition(1, worldpoint);

            // check if we are within bounds of the join target
            Image img = lineVertex.GetComponent<Image>();
        }
    }

    public void OnPointerEnter(LineVertex lineVertex, PointerEventData ptrData)
    {
        if (!enabled) return;

        if (_dragVertex != null && _dragVertex.joinTarget == lineVertex)
        {
            print("JOINED!");

            _joinLineRenderer.SetPosition(1, lineVertex.transform.position);
            enabled = false;
        }
    }
}
