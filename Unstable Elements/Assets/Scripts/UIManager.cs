﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [SerializeField] private Animator WinScreen;
    [SerializeField] private Animator LoseScreen;
    [SerializeField] private Animator TimeUpScreen;
    [SerializeField] public GameObject LivesRepository;
    [SerializeField] public GameObject TutorialsRepository;
    [SerializeField] private Image[] Lives;
    [SerializeField] private Sprite LostLife;
    [SerializeField] private Animator[] Tutorials;
    [SerializeField] private Sprite[] _victoryImages;
    [SerializeField] private Sprite[] _lossImages;
public Button PauseButton;

    private int lives;

    // Use this for initialization
    void Awake ()
    {
        Lives = LivesRepository.GetComponentsInChildren<Image>();
        lives = Lives.Length;
        Tutorials = TutorialsRepository.GetComponentsInChildren<Animator>();
    }

    public int ReturnLives()
    {
        return Lives.Length;
        
    }

    // Strike one life off
    public void DecrementLife(int life)
    {
        if (lives - 1 < 0) return;
        Lives[lives-1].sprite = LostLife;
        lives--;
    }

    // Open a particular message
    public void OpenUI(int level)
    {
        GameManager _gameManager = GetComponentInParent<GameManager>();
        switch (level)
        {
            case 0:
                int temp = Random.Range(0, _victoryImages.Length);
                switch (temp)
                {
                    case 0:
                        _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.Chemicool);
                        break;
                    case 1:
                        _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.Explosive);
                        break;
                    case 2:
                        _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.Great);
                        break;
                    case 3:
                        _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.WhatAReaction);
                        break;
                }
                WinScreen.GetComponent<Image>().sprite =_victoryImages[temp];
                WinScreen.SetBool("Open", true); 
                break;
            case 1:
                LoseScreen.SetBool("Open", true);
                _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.Youach);
                break;
            case 2:
                TimeUpScreen.SetBool("Open", true);
                _gameManager.AudioManager.AudioSource.PlayOneShot(_gameManager.AudioManager.Youach);
                break;
        }
    }

    //Switch off all UI
    public void ResetUI()
    {
        WinScreen.SetBool("Open", false);
        LoseScreen.SetBool("Open", false);
        TimeUpScreen.SetBool("Open", false);
    }

    public void OpenTutorial(int level)
    {
        if (level >= Tutorials.Length) return;
        Tutorials[level].SetBool("Open", true);
    }

    public void ResetTutorials()
    {
        foreach (var tutorial in Tutorials)
        {
            tutorial.SetBool("Open", false);
        }
    }
}
