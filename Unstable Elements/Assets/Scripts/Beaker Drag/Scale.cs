﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Scale : MonoBehaviour
    {
        [SerializeField] private Text _scaleText;
            [SerializeField] private AudioClip _placedSound;
         public bool Set, Finished;
        private float _maxNumber;
        private float _scaleNumber;
        

        // Use this for initialization
        void Start ()
        {
            // Choose random flask weight
            _maxNumber = Random.Range(0.09f, 0.110f);
            // Reset counter
            _scaleNumber = 0.000f;
        }
	
        // Update is called once per frame
        void Update ()
        {
            // If the player has won don't update
            if (Finished) return;


            var temp = Random.Range(0.007f, 0.0016f);

            // If the beaker is on the scale increment the weight else decrease it
            if (Set)
            {
                if (_scaleNumber <= _maxNumber)
                    _scaleNumber += temp;
            }
            else
            {
                if (_scaleNumber > 0)
                    _scaleNumber -= temp;
                else
                    _scaleNumber = 0;
            }

            // Ensure the weight is never negative
            if (_scaleNumber < 0)
                _scaleNumber = 0;

            // Display counter with 4 digits
            _scaleText.text = _scaleNumber.ToString("F4");
        }

        // Reset counter. If this is done when the beaker is on the scale bring forth victory
        public void ResetCounter()
        {
            _scaleText.text = "0.000";
            _scaleNumber = 0.000f;

            if (!Set) return;
            Finished = true;
            FindObjectOfType<GameManager>().WinGame();
        }

        // Check if the beaker is on the scale
        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.name == "Beaker" && Set != true)
            {
                Set = true;
                GetComponent<AudioSource>().PlayOneShot(_placedSound);
            }
        }

        public void OnCollisionExit2D(Collision2D collision)
        {
            Set = false;
        }
    
    }
}
