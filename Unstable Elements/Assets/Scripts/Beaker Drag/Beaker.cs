﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Beaker : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private Vector2 _originalPosition = -Vector2.one
            ; //Used to store location of screen touch origin for mobile controls.


        [SerializeField] private float KillSpeed;
        [SerializeField] private float RotationSpeed;
        [SerializeField] private float RotationKillSpeed;
        [SerializeField] private AudioClip _beakerSmash;
        [SerializeField] private AudioClip _beakerClink;
        private bool _grabbed, _onGround, _finished;
        private Vector2 _velocity;
        private Rigidbody2D _rb2D;
        private Scale _scale;
      

        // Use this for initialization
        void Start()
        {
            _rb2D = GetComponent<Rigidbody2D>();
            _rb2D.velocity = new Vector2(0, 0);
            _scale = FindObjectOfType<Scale>();
            
        }

        // Grab object
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (_scale.Set) return;
            _grabbed = true;
            _onGround = false;
        }

        // Stop existing velocity and gravity
        public void OnDrag(PointerEventData eventData)
        {
            if (_scale.Set) return;
            _rb2D.velocity = new Vector2(0, 0);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_scale.Set) return;
            // Reactivate gravity and stop grabbing object
            _grabbed = false;
            // Continue moving the object 
            if (_onGround)
                _rb2D.velocity = _velocity;
            else
                _rb2D.velocity = _velocity * 15;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            // Ensure Object stays at mouse position
            if (_grabbed)
                transform.position = Input.mousePosition;
            else if (Math.Abs(_velocity.x) > 0.5f || Math.Abs(_velocity.y) > 0.5f)
                _rb2D.AddForce(new Vector2(_velocity.x * 20, GetComponent<Rigidbody2D>().velocity.y));

            _velocity = new Vector2(transform.position.x, transform.position.y) - _originalPosition;

            // Rotate the beaker by its current velocity 
            if (Math.Abs(_velocity.x) > 0.5f)
            {
                transform.Rotate(new Vector3(0, 0, -_velocity.x / RotationKillSpeed));
            }

            if (Math.Abs(transform.localEulerAngles.z) > 0.5f)
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y,
                    Mathf.LerpAngle(transform.localEulerAngles.z, 0, RotationSpeed * Time.deltaTime));
                _originalPosition = transform.position;
            }


            // If on the ground freeze rotation to avoid the trembling problem
            _rb2D.freezeRotation = _onGround;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            //If you hit the floor or the scale settle down
            if (collision.transform.name == "Desk" || collision.transform.name == "Scale")
            {
                _rb2D.velocity = Vector2.zero;
                _rb2D.angularVelocity = 0;
                _onGround = true;
            }
            // If you hit a wall you will be pushed out
            else if (collision.transform.name == "Wall")
            {
                _rb2D.AddForce(new Vector2(-_velocity.x * 2, GetComponent<Rigidbody2D>().velocity.y));
                _grabbed = false;

                if (_velocity.x > KillSpeed || _velocity.y > KillSpeed)
                {
                    GetComponent<AudioSource>().PlayOneShot(_beakerSmash);
                    FindObjectOfType<GameManager>().LoseGame();
                    Destroy(this.gameObject);
                }
                else
                    GetComponent<AudioSource>().PlayOneShot(_beakerClink);
            }
            // If you collide with an obstacle die if yyou are going fast
            else if (collision.transform.name == "Obstacle")
            {
                if (_velocity.x > KillSpeed || _velocity.y > KillSpeed)
                {
                    GetComponent<AudioSource>().PlayOneShot(_beakerSmash);
                    FindObjectOfType<GameManager>().LoseGame();
                    Destroy(this.gameObject);
                }
                else
                    GetComponent<AudioSource>().PlayOneShot(_beakerClink);
            }
        }

       

      
    }
}
