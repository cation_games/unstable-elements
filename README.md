# Unstable Elements
A interdisciplinary group project for Abertay University Serious Game Jam


Produced by CATION GAMES:

||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

Cari Erica Watterton - Artist

Katy Wood - Artist, Animator

Charlie Gillies - Programmer

Thomas Dixon - Producer, Audio & Systems Designer

Charalampos Koundourakis - Programmer

||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
